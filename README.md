(papagei)
=========

![alt text](papagei.jpg)

Papagei is a simple german to english translation command-line utility.

Usage
-----
```
$ csi -s papagei.scm [search-term]
```

Options
-------
```
-h  --help        show this text
-e  --englisch    search using english as the input
-m  --max-amount  max number of search results per category (default: 3)
-r  --repl        start a repl to repeatedly search words with the supplied arguments
```
