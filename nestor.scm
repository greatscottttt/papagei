#! /usr/local/bin/csi -script

(use medea srfi-18)
;; (use srfi-18)

;;(format (current-error-port) "wat")
;;(format (current-error-port) "halloooooooooo")
;;(format (current-error-port) "before let:")
;;(write-json '((text . "bitchin!")))

;; works
;;(define (consumer)
;;  (make-thread
;;    (lambda ()
;;      (let loop ()
;;       (let ((text (read-char (current-input-port))))
;;        (when (not (eof-object? text))
;;          (format (current-error-port) "~A" text)))
;;       (loop)))))

(define (cp stuff)
  (format (current-error-port) "~A" stuff))



(define (collect-chars)
  (list->string
    (let loop ((len (+ 3 (read-byte))))
     (if (> len 0)
         (let ((c (read-char)))
          (if (eof-object? c)
              '()
              (cons c (loop (- len 1)))))
         '()))))


(let loop ()
 (let ((results (collect-chars)))
  (when (not (null? results))
    (cp results)))
 (loop))
 ;;(cp (medea#read-json results))
