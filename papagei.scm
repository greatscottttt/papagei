(use
  html-parser
  txpath
  srfi-37
  (only regex string-substitute)
  (only http-client with-input-from-request)
  parley)


;; helpers
(define (find-spacing-amt items last-value)
  (if (null? items)
      last-value
      (if (< last-value (string-length (car (car items))))
          (find-spacing-amt (cdr items) (string-length (car (car items))))
          (find-spacing-amt (cdr items) last-value))))


(define (create-final-list list-of-pairs)
  (define pred
    (lambda (thing)
      (and (string? thing)
           (not (string-every char-set:whitespace thing)))))

  (if (null? list-of-pairs)
      '()
      (let ((pair (car list-of-pairs)))
       (cons
         (map (lambda (a) (string-join (filter pred (flatten a)) "")) pair)
         (create-final-list (cdr list-of-pairs))))))


(define (nice-take items amount)
  (if (and (not (null? items))
           (>= (length items) amount))
      (take items amount)
      items))

;; smarter initial parsing

;; start with identifying the data-dz-name sections:
;;    verb
;;    example
;;    adjadv? - adjective / adverb
;;    subst   - substantive
;;    praep   - preposition
;;    phrase

;;(define divs ((txpath "//div[@data-dz-name='verb']") parsed-html))
;;(define divs ((txpath "//div[@data-dz-name='adjadv']") parsed-html))
;;(define divs ((txpath "//div[@data-dz-name='subst']") parsed-html))
;;(define divs ((txpath "//div[@data-dz-name='praep']") parsed-html))
;;(define divs ((txpath "//div[@data-dz-name='phrase']") parsed-html))

;; both languages
;; (define tables ((txpath "//td[@lang]") divs))
;; just deutsch
;; (define tables ((txpath "//td[@lang='de']") divs))

(define (row-pred list-or-string)
  (if (string? list-or-string)
      #t
      (if (and (not (symbol? list-or-string))
               (or
                 (equal? (car list-or-string) 'b)
                 (equal? (car list-or-string) 'small)))
          #t
          #f)))


;; Display output
(define (print-dict-results results)
  (let* ((spacing (find-spacing-amt (last results) 2))
         (join-proc
           (lambda (pair)
             (let ((joint (string-join (list
                            "~s"
                            (string-pad "" (- spacing (string-length (car pair))))
                            "~s"))))
               joint)))
         (title (car results))
         (tables (last results)))

    (when (not (null? tables))
      (print "-------------------- " title " --------------------")
      (for-each
        (lambda (pair)
          (print (format
                   (join-proc pair)
                   (car pair)
                   (last pair))))
        tables)
      (print ""))))


(define BASE-URL "http://dict.leo.org")
(define NO-TRANSLATION-FOUND "Leider konnte ich keine Übersetzung finden.")
(define QUIT ",q")

(define (request-and-parse-html word translation-direction)
  (let* ((request (string-concatenate `(,BASE-URL "/" ,translation-direction "/" ,word ".html")))
         (html (call/cc (lambda (k)
                      (with-exception-handler
                        (lambda (x) (k (begin (print NO-TRANSLATION-FOUND) '())))
                        (lambda () (with-input-from-request request #f read-string)))))))
    (when (not (null? html))
     (html->sxml html))))


(define (dict-search word translation-direction max-amount)

  ;; assume the entry exists for now
  (define (part-of-speech-table part parsed-html)
    ((txpath "//td[@lang]")
     ((txpath (string-concatenate `("//div[@data-dz-name='" ,part "']"))) parsed-html)))

  (define (get-output found-tables amount-per-list)
    (let ((answer (map (lambda (row) (filter row-pred row)) found-tables)))
      (nice-take (create-final-list (chop answer 2)) amount-per-list)))

  (define (sanitize word)
    (string-substitute "ß" "ss"
    (string-substitute "ä" "ae"
    (string-substitute "ö" "oe"
    (string-substitute "ü" "ue" word)))))

  (let* ((parsed-html (request-and-parse-html (sanitize word) translation-direction))
         (substantive (get-output (part-of-speech-table "subst" parsed-html) max-amount))
         (verb (get-output (part-of-speech-table "verb" parsed-html) max-amount))
         (adjadv (get-output (part-of-speech-table "adjadv" parsed-html) max-amount))
         (preposition (get-output (part-of-speech-table "praep" parsed-html) max-amount))
         (phrase (get-output (part-of-speech-table "phrase" parsed-html) max-amount)))

    (list
      `("Substantive" ,substantive)
      `("Verb" ,verb)
      `("Adjective" ,adjadv)
      `("Preposition" ,preposition)
      `("Phrase" ,phrase))))


(define help-text
  "Usage: papagei [search-term]
   -h  --help        show this text
   -e  --englisch    search using english as the input
   -m  --max-amount  max number of search results per category (default: 3)
   -r  --repl        start a repl to repeatedly search words with the supplied arguments (type ',q' to quit")

(define options
  (list (option '(#\h "help") #f #f
                (lambda _ (print help-text) (exit)))
        (option '(#\e "englisch") #f #f
                (lambda (option name arg seed translation-direction max-amount repl)
                  (values seed "englisch-deutsch" max-amount repl)))
        (option '(#\m "max-amount") #t #f
                (lambda (option name arg seed translation-direction max-amount repl)
                  (values seed translation-direction arg repl)))
        (option '(#\r "repl") #f #f
                (lambda (option name arg seed translation-direction max-amount repl)
                  (values seed translation-direction max-amount #t)))))


(define (main args)
  (receive (search-term translation-direction max-amount repl)
    (args-fold args
               options
               (lambda (option name arg . seeds)
                 (error "unrecognized option:" name))
               (lambda (operand seed direction amount to-repl)
                 (values (cons operand seed) direction amount to-repl))
               '()
               "deutsch-englisch"
               "3"
               #f)

    (let loop ()
     (if (and (null? search-term) (not repl))
         (begin (print help-text) (exit))
         (let ((term (if repl (parley "> ") (car search-term))))
          (if (equal? term QUIT)
              (exit)
              (let ((results (dict-search
                               term
                               translation-direction
                               (string->number max-amount))))
                (for-each print-dict-results results)))))
     (when repl
       (loop)))))

(main (command-line-arguments))
